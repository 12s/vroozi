import React, { Component } from 'react'
import { Button, Form, Input, Radio, Label } from 'semantic-ui-react'

class DisplayComponent extends Component {

  state = {
    elemCount: 2,
    elemShape: "rect",
    error: false
  }

  handleLogout = () => {
    this.props.handleLogout()
  }

  handleChange = (event) => {
    const {value, name} = event.target

    if (value > 4 || value < 1) {
      this.setState({ error: true })
    } else if (value > 0 && value < 5) {
      this.setState({ error: false })
    }

    this.setState({ [name]: value })
  }

  handleToggle = (event) => {
    const {value, name} = event.currentTarget.firstChild
    this.setState({ [name]: value })
  }

  renderShapes = () => {
    let shapes = [];

    for(let i = 0; i < Math.min(this.state.elemCount, 4); i++) {
        shapes.push(<div key={i} className={this.state.elemShape + " shape"}></div>);
    }
    return shapes
  }

  render() {
    return (
      <div className="display-wrapper">
        <div className="display-header">
          <div>Logged in as <strong>{this.props.user}</strong></div>
          <Button
              type="button"
              onClick={this.handleLogout}
          >Log Out</Button>
        </div>


        <div className="canvas">
          {this.renderShapes()}
        </div>

        <div className="controls">
          <div>
            <Label pointing='below'>Number of elements</Label>
            <div className={this.state.error ? "error" : ""}>
              <Input
                  type="text"
                  name="elemCount"
                  value={this.state.elemCount}
                  onChange={this.handleChange}
              >
              </Input>

            </div>
          </div>

          <Form>
            <Label pointing='below'>Element shape</Label>
            <Form.Field>
              <Radio
                  type="radio"
                  name="elemShape"
                  checked={this.state.elemShape === "circle"}
                  value="circle"
                  onChange={this.handleToggle}
                  label="Circle"
              />
            </Form.Field>
            <Form.Field>
              <Radio
                  type="radio"
                  name="elemShape"
                  checked={this.state.elemShape === "rect"}
                  value="rect"
                  onChange={this.handleToggle}
                  label="Rectangle"
              />
            </Form.Field>
          </Form>
        </div>
      </div>
    )
  }
}

export default DisplayComponent;
