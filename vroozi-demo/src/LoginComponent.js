import React, { Component } from 'react'
import { Button, Form } from 'semantic-ui-react'

class LoginComponent extends Component {

  state = {
    email: "",
    password: ""
  }

  handleSubmit = (event) => {
    fetch(`https://jsonplaceholder.typicode.com/users`)
      .then(response => response.json())
      .then(response => {
        let {email, password} = this.state

        response.filter((person) => {
          if(person.email === email && person.address.city === password) {
            this.props.handleLogin(person)
            return true
          }
        })
      })
  }

  handleChange = (event) => {
    const {name, value} = event.target
    this.setState({
      [name]: value
    })
  }

  render() {
    return (
        <div className="login-wrapper">
        <Form>
          <Form.Field>
            <label>Email</label>
            <input
                type="text"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
            />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <input
                type="password"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
            />
          </Form.Field>

          <Button
            onClick={this.handleSubmit}>
            Log In
          </Button>
        </Form>




        </div>
    )
  }
}

export default LoginComponent;
