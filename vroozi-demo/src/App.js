import React, { Component } from 'react'
import LoginComponent from "./LoginComponent"
import DisplayComponent from "./DisplayComponent"

class App extends Component {

  state = {
    isLoggedIn: false
  }

  handleLogin = (person) => {
      this.setState({
        isLoggedIn: true,
        user: person.name
      })
  }

  handleLogout = () => {
      this.setState({ isLoggedIn: false})
  }

  render() {
    return (
      <div>
        {this.state.isLoggedIn ?
          <DisplayComponent
                handleLogout={this.handleLogout}
                user={this.state.user}/>
          : <LoginComponent
                handleLogin={this.handleLogin}/>}
      </div>
    )
  }
}

export default App;
